-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2019 at 11:28 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm_adawiyah`
--

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `kode_jenis` int(11) NOT NULL,
  `kode_kategori` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon` double NOT NULL,
  `limited_diskon` datetime DEFAULT NULL,
  `deskripsi` longtext NOT NULL,
  `date_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `foto_1` text NOT NULL,
  `foto_2` text NOT NULL,
  `foto_3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `kode_jenis`, `kode_kategori`, `nama`, `harga`, `diskon`, `limited_diskon`, `deskripsi`, `date_add`, `foto_1`, `foto_2`, `foto_3`) VALUES
(1, 7, 7, 'Antihemoroid', 30000, 0, NULL, '10 Suppository per dus', '2019-03-10 06:21:13', '1_1.jpg', '1_2.png', '1_3.png'),
(2, 1, 13, 'Asifit Kaplet Blister', 55909, 75, '2019-03-18 15:48:00', '30 kaplet per botol', '2019-03-16 06:21:13', '2_1.jpg', '2_2.png', '2_3.png'),
(3, 3, 6, 'Aspilets Chewable', 53000, 0, NULL, '100 tablet per dus', '2019-03-10 06:21:13', '3_1.jpg', '3_2.png', '3_3.png'),
(4, 3, 6, 'Aspilets Thrombo Tablet', 106313, 0, NULL, '150 tablet per dus', '2019-03-10 06:21:13', '4_1.jpg', '4_2.png', '4_3.png'),
(5, 8, 1, 'Batugin Elixir', 37309, 0, NULL, '300 ml per botol', '2019-03-16 06:21:13', '5_1.jpg', '5_2.jpg', '5_3.png'),
(6, 6, 11, 'Bedak Salicyl', 5460, 0, NULL, 'Bedak Salicyl 2% 60 gr', '2019-03-10 06:21:13', '6_1.jpg', '6_2.png', '6_3.png'),
(7, 9, 7, 'Betadine Feminine Hygiene', 20400, 0, NULL, 'Betadine Feminine Hygiene 60 ml', '2019-03-10 06:21:13', '7_1.jpg', '7_2.png', '7_3.png'),
(8, 9, 7, 'Betadine Obat Kumur 100 ml', 11030, 0, NULL, 'Betadine Obat Kumur 100 ml', '2019-03-10 06:21:13', '8_1.jpg', '8_2.png', '8_3.png'),
(9, 9, 7, 'Betadine Obat Kumur 190 ml', 18640, 0, NULL, 'Betadine Obat Kumur 190 ml', '2019-03-10 06:21:13', '9_1.jpg', '9_2.png', '9_3.png'),
(10, 9, 7, 'Betadine Solution 5 ml', 3310, 0, NULL, 'Betadine Solution 5 ml', '2019-03-16 06:21:13', '10_1.jpg', '10_2.png', '10_3.png'),
(11, 9, 7, 'Betadine Solution 15 ml', 9400, 0, NULL, 'Betadine Solution 15 ml', '2019-03-10 06:21:13', '11_1.jpg', '11_2.png', '11_3.png'),
(12, 3, 6, 'Biogesic Blister', 39600, 0, NULL, 'Biogesic Blister 100 tablet per dus', '2019-03-10 06:21:13', '12_1.png', '12_2.png', '12_3.png'),
(13, 8, 7, 'Bisovon Extra Syrup', 34000, 0, NULL, 'Bisolvon Extra Syrup 60 ml', '2019-03-10 06:21:13', '13_1.jpg', '13_2.png', '13_3.png'),
(14, 6, 6, 'Cendo Cenfresh Minidose', 23000, 0, NULL, 'Cendo Cenfresh Minidose 5 × 0,6 ml per strip', '2019-03-10 06:21:13', '14_1.jpg', '14_2.jpg', '14_3.png'),
(15, 10, 6, 'Cendo Lyteers Eye Drops', 22500, 0, NULL, 'Cendo Lyteers Eye Drops 15 ml', '2019-03-16 06:21:13', '15_1.jpg', '15_2.png', '15_3.png'),
(16, 6, 6, 'Cendo Lyteers Minidose', 19250, 0, NULL, 'Cendo Lyteers Minidose 5 × 0,6 ml', '2019-03-10 06:21:13', '16_1.jpg', '16_2.png', '16_3.png'),
(17, 8, 13, 'Curcuma+ Grow Strawberry', 21000, 0, NULL, 'Curcuma+ Grow Strawberry  200 ml', '2019-03-10 06:21:13', '17_1.jpg', '17_2.png', '17_3.png'),
(18, 3, 13, 'Enervon C', 27189, 0, NULL, 'Enervon C tablet 30', '2019-03-10 06:21:13', '18_1.jpg', '18_2.png', '18_3.png'),
(19, 3, 13, 'Enervon C FC', 92319, 0, NULL, 'Enervon C FC tablet 100', '2019-03-10 06:21:13', '19_1.jpg', '19_2.png', '19_3.png'),
(20, 8, 1, 'Enkasari Cairan 120 ml', 16237, 0, NULL, 'Enkasari Cairan 120 ml per botol', '2019-03-16 06:21:13', '20_1.jpg', '20_2.png', '20_3.png'),
(21, 10, 7, 'Insto Regular', 10600, 0, NULL, 'Insto Regular 7,5 ml', '2019-03-10 06:21:13', '21_1.jpg', '21_2.png', '21_3.png'),
(22, 9, 7, 'Iodyne Povidone 10% 30ml', 4500, 0, NULL, 'Iodyne Povidone 10% 30 ml', '2019-03-10 06:21:13', '22_1.jpg', '22_2.png', '22_3.png'),
(23, 9, 7, 'Iodyne Povidone 10% 60ml', 6550, 0, NULL, 'Iodyne Povidone 10% 60 ml', '2019-03-10 06:21:13', '23_1.jpg', '23_2.png', '23_3.png'),
(24, 9, 11, 'Lactacyd Baby', 20900, 0, NULL, 'Lactacyd Baby 60 ml', '2019-03-10 06:21:13', '24_1.jpg', '24_2.png', '24_3.png'),
(25, 9, 11, 'Lactacyd Woman', 19800, 0, NULL, 'Lactacyd Woman 60 ml', '2019-03-16 06:21:13', '25_1.jpg', '25_2.jpg', '25_3.png'),
(26, 8, 6, 'Magasida 60 ml Suspensi', 23545, 0, NULL, 'Magasida 60 ml Suspensi', '2019-03-10 06:21:13', '26_1.jpg', '26_2.png', '26_3.png'),
(27, 3, 6, 'Magasida Tablet', 82545, 0, NULL, 'Magasida Tablet 100 butir', '2019-03-10 06:21:13', '27_1.jpg', '27_2.jpg', '27_3.jpg'),
(28, 2, 11, 'Marcks Bedak Creme', 11560, 0, NULL, 'Marcks Bedak Creme', '2019-03-10 06:21:13', '28_1.jpg', '28_2.png', '28_3.png'),
(29, 2, 11, 'Marcks Bedak Rose', 11560, 0, NULL, 'Marcks Bedak Rose', '2019-03-10 06:21:13', '29_1.jpg', '29_2.jpg', '29_3.JPG'),
(30, 4, 7, 'Miconazole 2% 10 gr', 3250, 0, NULL, 'Miconazole 2% 10 gr (24 tube per dus)', '2019-03-16 06:21:13', '30_1.jpg', '30_2.png', '30_3.png'),
(31, 8, 6, 'Naprex Drops Tst', 36800, 0, NULL, 'Naprex Drops Tst 60 mg / 15 ml', '2019-03-10 06:21:13', '31_1.jpg', '31_2.png', '31_3.png'),
(32, 8, 6, 'Naprex Tst Rite', 34300, 0, NULL, 'Naprex Tst Rite 250 mg / 60 ml', '2019-03-10 06:21:13', '32_1.jpg', '32_2.png', '32_3.png'),
(33, 1, 13, 'Natur-E Advanced 16s', 32729, 0, NULL, 'Natur-E Advanced 16s 16 kapsul', '2019-03-10 06:21:13', '33_1.png', '33_2.jpg', '33_3.png'),
(34, 11, 11, 'Natur-E Advanced HBL 100 ml', 14832, 0, NULL, 'Natur-E Advanced HBL 100 ml', '2019-03-10 06:21:13', '34_1.jpg', '34_2.jpg', '34_3.png'),
(35, 11, 11, 'Natur-E Advanced HBL 245 ml', 27810, 0, NULL, 'Natur-E Advanced HBL 245 ml', '2019-03-16 06:21:13', '35_1.jpg', '35_2.jpg', '35_3.png'),
(36, 11, 12, 'Natur-E Daily Nourishing Energizing', 12220, 0, NULL, 'Natur-E Daily Nourishing Energizing 100 ml', '2019-03-10 06:21:13', '36_1.jpg', '36_2.jpg', '36_3.png'),
(37, 11, 11, 'Natur-E Daily NR Lot 100 ml', 12220, 0, NULL, 'Natur-E Daily NR Lot 100 ml', '2019-03-10 06:21:13', '37_1.png', '37_2.jpg', '37_3.jpg'),
(38, 11, 11, 'Natur-E Daily NR Lotion 245 ml', 24517, 0, NULL, 'Natur-E Daily Nourishing Lotion 245 ml', '2019-03-10 06:21:13', '38_1.jpg', '38_2.png', '38_3.png'),
(39, 11, 11, 'Natur-E HBL 100 ml', 10424, 0, NULL, 'Natur-E HBL 100 ml', '2019-03-10 06:21:13', '39_1.jpg', '39_2.png', '39_3.png'),
(40, 1, 13, 'Natur-E Soft Capsul', 15134, 0, NULL, 'Natur-E Soft Capsul 16', '2019-03-16 06:21:13', '40_1.jpg', '40_2.png', '40_3.png'),
(41, 8, 6, 'OBH Combi Batuk Berdahak Menthol', 8750, 0, NULL, 'OBH Combi Batuh Berdahak Menthol', '2019-03-10 06:21:13', '41_1.png', '41_2.png', '41_3.png'),
(42, 8, 6, 'OBH Combi Batuk Berdahak Menthol', 9200, 0, NULL, 'OBH Combi Batuk Berdahak Menthol\r\nNew Stock', '2019-03-10 06:21:13', '42_1.png', '42_2.png', '42_3.png'),
(43, 8, 7, 'OBH Combi Batuk Flu Menthol 60 ml', 9450, 0, NULL, 'OBH Combi Batuk Flu Menthol 60 ml', '2019-03-10 06:21:13', '43_1.jpg', '43_2.png', '43_3.png'),
(44, 8, 7, 'OBH Combi Batuk Flu Menthol 100 ml', 12500, 0, NULL, 'OBH Combi Batuk Flu Menthol 100 ml', '2019-03-10 06:21:13', '44_1.jpg', '44_2.png', '44_3.png'),
(45, 11, 7, 'Peditox 1% 50 ml', 5550, 0, NULL, 'Peditox 1% 50 ml adalah larutan/lotion pembasmi kutu rambut', '2019-03-16 06:21:13', '45_1.jpg', '45_2.jpg', '45_3.png'),
(46, 11, 7, 'Peditox 1% 50 ml (new stock)', 6000, 0, NULL, 'Peditox 1% 50ml adalah larutan pembasmi kutu rambut', '2019-03-10 06:21:13', '46_1.jpg', '46_2.jpg', '46_3.png'),
(47, 1, 13, 'Pharmaton Formula', 191126, 0, NULL, 'Pharmaton Formula 50 kapsul per dus', '2019-03-10 06:21:13', '47_1.jpg', '47_2.png', '47_3.png'),
(48, 10, 7, 'Rohto Cool', 13320, 0, NULL, 'Rohto Cool 7 ml', '2019-03-10 06:21:13', '48_1.jpg', '48_2.png', '48_3.png'),
(49, 9, 6, 'Salicyl Fresh Kemasan Baru', 12380, 0, NULL, 'Salicyl Fresh 60 gr', '2019-03-10 06:21:13', '49_1.jpg', '49_2.png', '49_3.png'),
(50, 8, 1, 'Silex Syrup', 45413, 0, NULL, 'Silex Syrup 100 ml', '2019-03-16 06:21:13', '50_1.jpg', '50_2.jpg', '50_3.png'),
(51, 3, 13, 'Vicee Orange', 57173, 0, NULL, 'Vicee Orange 100 tablet per dus', '2019-03-10 06:21:13', '51_1.jpg', '51_2.jpg', '51_3.jpg'),
(53, 3, 6, 'Vitamin C 100 mg', 11040, 0, NULL, 'Vitamin C 100 mg', '2019-03-10 06:21:13', '52_1.jpg', '52_2.jpg', '52_3.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `kode_jenis` (`kode_jenis`),
  ADD KEY `kode_kategori` (`kode_kategori`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`kode_kategori`) REFERENCES `kategori_produk` (`kode_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`kode_jenis`) REFERENCES `jenis_produk` (`kode_jenis`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
