<!DOCTYPE html>
<html>
<head>
	<title>Latihan Datatable</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/media/css/dataTables.bootstrap.css') ?>">

<style type="text/css">

.container{
	margin-top: 50px;
}
.redBG{
	background-color:RED !important;
}

</style>

</head>
<body>

<div class="container">
<div class="row">
<div class="col-md-9">

<table class="table table-striped table-desa">
<thead>
	<tr>
		<th style="width:50px:color:black;">No</th>
		<th>Nama Barang</th>
		<th>Harga Barang</th>
		<th>Desk</th>
		<th>Act</th>
	</tr>
	<tr>
		<th>No</th>
		<th class="col_searchable">Nama Barang</th>
		<th class="col_searchable">Harga Barang</th>
		<th class="col_searchable">Desk</th>
		<th>Act</th>
	</tr>
</thead>
<tbody>
</tbody>
<tfoot>
	<tr>
		<th>No</th>
		<th>Nama Barang</th>
		<th>Harga Barang</th>
		<th>Desk</th>
		<th>Act</th>
	</tr>
</tfoot>
</table>

</div>
</div>
</div>



<script type="text/javascript" src="<?php echo base_url('assets/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/media/js/dataTables.bootstrap.min.js') ?>"></script>

<script type="text/javascript">
//add text input foreach columns
$(".table-desa thead tr:eq(1) th").each(function(){
	var title = $(this).text();
	//if(title == 'No' || title == 'Act'){
	if($(this).hasClass("col_searchable")){
		$(this).html('<input type="text" class="col_search" placeholder="Search '+title+'" />');		
	}else{
		$(this).html('&nbsp;');		
	}
});

//datatable
var table =
$(".table-desa").DataTable({
	columnDefs:[
		{name:'no', data:'no_rs', targets:0,orderable: false,sortable:false},
		{name:'nama', data:'nama_rs', 'targets':1},
		{name:'harga', data:'harga_rs', 'targets':2},
		{name:'deskripsi', data:'deskripsi_rs', 'targets':3,searchable:false,visible: false},
		{
			name:'act', data:'id_produk_rs', targets:4,orderable: false,sortable:false
			,render: function(data,type,row,meta){
				return "<button id='"+data+"'>"+data+"</button>";
			}
		},
	],
	"createdRow": function( row, data, dataIndex ) {
		if ( data["id_produk_rs"] == "2" ) {
			$( row ).css( "background-color", "Orange" );
			$( row ).addClass( "warning" );
		}
	},
	orderCellsTop: true, //make order appear on top tr of thead
	orderMulti: true,
	ordering: true,
	order:[[1,'asc']],
	processing: true,
	serverSide: true,
	//bFilter: false, //remove search functionability
	sDom: 'lrtip', //hide searchbox 
	ajax: {
	  url: "<?php echo base_url('index.php/produk/get_data') ?>",
	  type:'POST',
	  data:function(d){
		  d.tm_custom = "tm custom val";
		  d.tm_custom2 = "tm custom val2";
		  d.tm_custom_arr = [{data:'val 1'},{data:'val 2'}];
	  }
	},
});


//apply search on column
//$('.table-desa thead').on("keyup",".col_search",function(){
$('.table-desa thead').on("keyup",".col_search",function(event){
	if(event.which == 13){ //enter
		table.column($(this).parent().index())
			.search(this.value)
			.draw();
	}
});
</script>


</body>
</html>