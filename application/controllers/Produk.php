<?php
class Produk extends CI_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}
	
	public function index(){
		$this->load->view("table_produk");
	}
	
	public function get_data(){
		//read doc https://datatables.net/manual/server-side
		
		//token
		$draw = $_POST["draw"];
		//num of row to be shown per page
		$length = $_POST["length"];
		//offset, tell database get data from what #index
		$start = $_POST["start"];
		//search keyword
		$search = $_POST["search"]["value"];
		//datatable columns
		$columns = $_POST["columns"];
		//datatable orders
		$orders = $_POST["order"];
		
		//order column
		//$columns = array("nama","harga","deskripsi");
		$order = "nama";
		if($_POST["order"][0]["column"]>0){
			$col_index = $_POST["order"][0]["column"];
			//$order = $columns[$_POST["order"][0]["column"]-1];
			$order = $_POST["columns"][$col_index]["name"];
		}
		//order directions
		$dir = "asc";
		if(!empty($_POST["order"][0]["dir"])){
			$dir = $_POST["order"][0]["dir"];
		}
		
		//count total records
		$total = $this->db->count_all_results("produk");
		
		//output data to send to the client
		$output = array();
		
		//token from client, will be send back to client
		//RECOMMENDED TO CAST IT AS INTEGER INSTEAD ECHOING BACK TO CLIENT
		//TO PREVENT XSS ATTACK
		$output["draw"] = (int) $draw;
		
		//$output["recordsTotal] is total data before filtering
		//$output["recordsFiltered"] is total data aftar filtered
		//they often have same value, except search
		$output["recordsTotal"] = $output["recordsFiltered"] = $total;
		
		//the output data to be shown on client table
		$output["data"]=array();
		
		//if search is not null then
		if(!empty($search)){
			//count filtered data
			$num = 
				$this->db->like("nama",$search)
					->get("produk");
			$output["recordsFiltered"] = $num->num_rows();
			
			//filtering data
			$this->db->like("nama",$search);
		}
		
		//if search per column not null
		$col_search = array();
		foreach($columns AS $column){
			if(!empty($column["search"]["value"])){
				$col_search[$column["name"]] = $column["search"]["value"];
			}
		}
		if(!empty($col_search)){
			//count filtered data
			$num = 
				$this->db->group_start() //grouping dalam kurung caknya
						->like($col_search)
					->group_end()
					->get("produk");
			$output["recordsFiltered"] = $num->num_rows();
			
			//filtering data
			$this->db->like($col_search);
		}
		
		//perform query
		$this->db->limit($length,$start);
		//order by alphabet
		if(count($orders)>0){
			foreach ($orders as $order) {
				# code...
				$this->db->order_by($columns[$order["column"]]["name"],$order["dir"]);
			}
		}
			
		$query =$this->db->get("produk");
		
		
		$number = $start+1;
		foreach($query->result_array() AS $row){
			$output["data"][] = array(
									"no_rs"=>$number++,
									"deskripsi_rs"=>$row["deskripsi"],
									"nama_rs"=>$row["nama"],
									"harga_rs"=>$row["harga"],
									"id_produk_rs"=>$row["id_produk"],
								);
		}
		
		//$output["error"] = "Ups ada error";
		
		echo json_encode($output);
	}
	
	public function test_data(){
		
		$query =
			$this->db->limit(40,0)
				//order by alphabet
				->order_by('nama','DESC')
				->get("produk");
				
		print_r($query->result());
	}
}