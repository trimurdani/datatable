-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2019 at 05:37 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm_adawiyah`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `role` enum('owner','admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_user`, `username`, `password`, `nama_user`, `role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `apotek`
--

CREATE TABLE `apotek` (
  `id_apotek` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apotek`
--

INSERT INTO `apotek` (`id_apotek`, `nama`, `alamat`, `id_user`) VALUES
(1, 'Apotek Bari', 'Palembang BARI', 3),
(2, 'Apotek Testing', 'Jl. KH. Azhari', 5),
(3, 'Apotek Al-Syifaa', 'JL. DR. Isa No. 112, Ilir Timur II Palembang', 6),
(4, 'Apotek Delima', 'JL. JENDRAL SUDIRMAN NO. 70/545 RT 008 RW 003', 7),
(5, 'Apotek Bunda (CV Bunda) Palembang', 'JL. S. PRAWIRO NO. 3796 RT. 037', 8),
(6, 'APOTEK KOMALA ', 'JL. H. SANUSI NO. 37 RT 037 RW 006 PALEMBANG', 9),
(7, 'APOTEK DINARA', 'JL. RESIDEN ABDUL ROZAK NO. 1 PALEMBANG', 10),
(8, 'APOTEK BUMI AYU PALEMBANG', 'JL. SULTAN M. BADARUDIN II KM PALEMBANG', 11),
(9, 'APOTEK GANDA PRIMA', 'JL. SMB II RT 001 KEL. SUKODADI KEC. SUKARAMI PALEMBANG', 12),
(10, 'APOTEK PANCA', 'JL. PANCA USAHA NO. 2076 RT 060 PALEMBANG', 13),
(11, 'APOTEK KALAM AYKIA', 'JL. KH. WAHID HASYIM NO. 259 RT 008 RW 002 JAKABARING', 14),
(12, 'APOTEK ROID', 'JL. KH. AZHARI NO. 25 PALEMBANG', 15),
(13, 'APOTEK RIZKI', 'JL. KH. AZHARI NO. 638 RT 053 RW 014', 16),
(14, 'APOTEK ADISYAH FARMA', 'JL. KH. WAHID HASYIM NO. 790 PALEMBANG', 17),
(15, 'user1 Apotek', 'Palembang', 1);

-- --------------------------------------------------------

--
-- Table structure for table `detil_pemesanan`
--

CREATE TABLE `detil_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detil_pemesanan`
--

INSERT INTO `detil_pemesanan` (`id_pemesanan`, `id_produk`, `harga`, `qty`, `catatan`) VALUES
(3, 1, 25000, 1, ''),
(3, 2, 5000, 1, ''),
(5, 1, 30000, 1, ''),
(5, 2, 55909, 1, ''),
(6, 5, 37309, 1, ''),
(6, 50, 45413, 1, ''),
(7, 17, 21000, 1, ''),
(8, 5, 37309, 1, ''),
(8, 50, 45413, 1, ''),
(9, 4, 106313, 1, ''),
(9, 15, 22500, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE `jenis_produk` (
  `kode_jenis` int(11) NOT NULL,
  `jenis_produk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_produk`
--

INSERT INTO `jenis_produk` (`kode_jenis`, `jenis_produk`) VALUES
(1, 'Kapsul'),
(2, 'Piece'),
(3, 'Tablet'),
(4, 'Tube'),
(6, 'Strip'),
(7, 'Suppository'),
(8, 'Syrup'),
(9, 'Obat Antiseptik'),
(10, 'Obat Tetes Mata'),
(11, 'Lotion');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_kuisioner`
--

CREATE TABLE `kategori_kuisioner` (
  `id_kategori_kuisioner` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_kuisioner`
--

INSERT INTO `kategori_kuisioner` (`id_kategori_kuisioner`, `nama_kategori`) VALUES
(1, 'Bukti Fisik (Tangibility)'),
(2, 'Kehandalan (Reability)'),
(3, 'Daya Tanggap (Responsiveness)'),
(4, 'Jaminan (Assurance)'),
(5, 'Kepedulian (Emphaty)');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `kode_kategori` int(11) NOT NULL,
  `kategori_produk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`kode_kategori`, `kategori_produk`) VALUES
(1, 'Jamu'),
(3, 'Make-up Base'),
(4, 'Obat Batuk'),
(6, 'Obat Bebas'),
(7, 'Obat Bebas Terbatas'),
(8, 'Obat Herbal Terstandar'),
(11, 'Personal Care'),
(12, 'Skin Care'),
(13, 'Supplement');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_user` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `komentar` longtext NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reply_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_produk`, `id_user`, `komentar`, `waktu`, `reply_to`) VALUES
(1, 5, 1, 'test tulis komentar user1 produk 5', '2019-03-24 17:47:14', NULL),
(2, 5, NULL, 'test reply komentar user1 produk 5', '2019-03-24 18:13:14', 1),
(3, 5, 1, 'test tulis komentar ke 2 user1 produk 5', '2019-03-24 18:36:14', NULL),
(4, 5, NULL, 'Test balas WEB', '2019-03-25 21:38:40', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner`
--

CREATE TABLE `kuisioner` (
  `id_pertanyaan` int(11) NOT NULL,
  `pertanyaan` varchar(100) NOT NULL,
  `id_kategori_kuisioner` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner`
--

INSERT INTO `kuisioner` (`id_pertanyaan`, `pertanyaan`, `id_kategori_kuisioner`, `count`, `jumlah`) VALUES
(1, 'Fasilitas ruang pelayanan yang baik dan memadai', 1, 1, 5),
(2, 'Kebersihan dan kenyamanan pada ruang pelayanan', 1, 2, 7),
(3, 'Fasilitas saranan pengiriman surat dan paket sudah memadai', 1, 2, 8),
(4, 'Sikap dan perhatian karyawan kepada masyarakat baik dan responsif dalam melayani', 1, 2, 9),
(5, 'Kemampuan pengiriman surat dan paket tepat waktu dan tepat pada orang yang menerimanya', 2, 2, 6),
(6, 'Pelayanan pengiriman dilakukan dengan baik dan cepat', 2, 2, 7),
(7, 'Kemampuan menjaga paket yang dikirim dalam keadaan utuh', 2, 2, 8),
(8, 'Kemampuan mendengarkan ketika pelanggan meminta pelayanan', 3, 2, 6),
(9, 'Pihak POS dapat menangani keluhan pelanggan', 3, 2, 7),
(10, 'Pihak POS memberikan layanan dengan cepat dan tepat terhadap masalah pelanggan', 3, 2, 8),
(11, 'Pengetahuan karyawan cukup baik dalam memberikan informasi yang dibutuhkan', 4, 2, 6),
(12, 'Bertanggung jawab terhadap keterlambatan pengiriman', 4, 2, 7),
(13, 'Memberikan garansi jika ada kerusakan pada saat pengiriman surat dan paket', 4, 2, 8),
(14, 'Memberikan jaminan bila ada keluhan dari pelanggan', 4, 2, 9),
(15, 'Keramahan dan tanggap yang baik dari karyawan saat ada pengaduan oleh pelanggan', 5, 2, 6),
(16, 'Terdapat sarana untuk menyampaikan keluhan dan saran', 5, 2, 7),
(17, 'Berusaha mengenal pelanggan dan memahami kebutuhan mereka', 5, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `kuisioner` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_user`, `username`, `password`, `nama_user`, `kuisioner`) VALUES
(1, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'User Satu', 1),
(3, 'user2', '7e58d63b60197ceb55a1c487989a3720', 'User ke Dua', 1),
(5, 'adawiyahwiwid', 'd272dd7eba4a87afc02d8e25cd474fbf', 'Wiwid', 0),
(6, 'rina', '81dc9bdb52d04dc20036dbd8313ed055', 'Amrina Rosyada', 0),
(7, 'ridho', '81dc9bdb52d04dc20036dbd8313ed055', 'Ridho Albani', 0),
(8, 'lisa', '81dc9bdb52d04dc20036dbd8313ed055', 'Lisa Ningtyas', 0),
(9, 'fadilah', '81dc9bdb52d04dc20036dbd8313ed055', 'Fadilah', 0),
(10, 'dina', '81dc9bdb52d04dc20036dbd8313ed055', 'Dina', 0),
(11, 'ayu', '81dc9bdb52d04dc20036dbd8313ed055', 'Ayu Dirgantari', 0),
(12, 'widya', '81dc9bdb52d04dc20036dbd8313ed055', 'Widya Astuti', 0),
(13, 'febri', '81dc9bdb52d04dc20036dbd8313ed055', 'Febrianto', 0),
(14, 'riyan', '81dc9bdb52d04dc20036dbd8313ed055', 'Riyan', 0),
(15, 'santi', '81dc9bdb52d04dc20036dbd8313ed055', 'Susanti', 0),
(16, 'rizki', '81dc9bdb52d04dc20036dbd8313ed055', 'Rizki', 0),
(17, 'yoga', '81dc9bdb52d04dc20036dbd8313ed055', 'Yoga', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `waktu_pemesanan` datetime NOT NULL,
  `status_pemesanan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_user`, `waktu_pemesanan`, `status_pemesanan`) VALUES
(3, 1, '2019-02-28 20:13:58', '1'),
(4, 1, '2019-03-02 04:06:19', '1'),
(5, 1, '2019-03-16 16:40:50', '1'),
(6, 1, '2019-03-16 20:37:15', '1'),
(7, 1, '2019-03-18 19:25:33', '1'),
(8, 1, '2019-03-18 19:28:21', '1'),
(9, 6, '2019-03-18 19:29:42', '1');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `kode_jenis` int(11) NOT NULL,
  `kode_kategori` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon` double NOT NULL,
  `limited_diskon` datetime DEFAULT NULL,
  `deskripsi` longtext NOT NULL,
  `date_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `foto_1` text NOT NULL,
  `foto_2` text NOT NULL,
  `foto_3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `kode_jenis`, `kode_kategori`, `nama`, `harga`, `diskon`, `limited_diskon`, `deskripsi`, `date_add`, `foto_1`, `foto_2`, `foto_3`) VALUES
(1, 7, 7, 'Antihemoroid', 30000, 0, NULL, '10 Suppository per dus', '2019-03-10 06:21:13', '1_1.jpg', '1_2.png', '1_3.png'),
(2, 1, 13, 'Asifit Kaplet Blister', 55909, 75, '2019-03-18 15:48:00', '30 kaplet per botol', '2019-03-16 06:21:13', '2_1.jpg', '2_2.png', '2_3.png'),
(3, 3, 6, 'Aspilets Chewable', 53000, 0, NULL, '100 tablet per dus', '2019-03-10 06:21:13', '3_1.jpg', '3_2.png', '3_3.png'),
(4, 3, 6, 'Aspilets Thrombo Tablet', 106313, 0, NULL, '150 tablet per dus', '2019-03-10 06:21:13', '4_1.jpg', '4_2.png', '4_3.png'),
(5, 8, 1, 'Batugin Elixir', 37309, 0, NULL, '300 ml per botol', '2019-03-16 06:21:13', '5_1.jpg', '5_2.jpg', '5_3.png'),
(6, 6, 11, 'Bedak Salicyl', 5460, 0, NULL, 'Bedak Salicyl 2% 60 gr', '2019-03-10 06:21:13', '6_1.jpg', '6_2.png', '6_3.png'),
(7, 9, 7, 'Betadine Feminine Hygiene', 20400, 0, NULL, 'Betadine Feminine Hygiene 60 ml', '2019-03-10 06:21:13', '7_1.jpg', '7_2.png', '7_3.png'),
(8, 9, 7, 'Betadine Obat Kumur 100 ml', 11030, 0, NULL, 'Betadine Obat Kumur 100 ml', '2019-03-10 06:21:13', '8_1.jpg', '8_2.png', '8_3.png'),
(9, 9, 7, 'Betadine Obat Kumur 190 ml', 18640, 0, NULL, 'Betadine Obat Kumur 190 ml', '2019-03-10 06:21:13', '9_1.jpg', '9_2.png', '9_3.png'),
(10, 9, 7, 'Betadine Solution 5 ml', 3310, 0, NULL, 'Betadine Solution 5 ml', '2019-03-16 06:21:13', '10_1.jpg', '10_2.png', '10_3.png'),
(11, 9, 7, 'Betadine Solution 15 ml', 9400, 0, NULL, 'Betadine Solution 15 ml', '2019-03-10 06:21:13', '11_1.jpg', '11_2.png', '11_3.png'),
(12, 3, 6, 'Biogesic Blister', 39600, 0, NULL, 'Biogesic Blister 100 tablet per dus', '2019-03-10 06:21:13', '12_1.png', '12_2.png', '12_3.png'),
(13, 8, 7, 'Bisovon Extra Syrup', 34000, 0, NULL, 'Bisolvon Extra Syrup 60 ml', '2019-03-10 06:21:13', '13_1.jpg', '13_2.png', '13_3.png'),
(14, 6, 6, 'Cendo Cenfresh Minidose', 23000, 0, NULL, 'Cendo Cenfresh Minidose 5 × 0,6 ml per strip', '2019-03-10 06:21:13', '14_1.jpg', '14_2.jpg', '14_3.png'),
(15, 10, 6, 'Cendo Lyteers Eye Drops', 22500, 0, NULL, 'Cendo Lyteers Eye Drops 15 ml', '2019-03-16 06:21:13', '15_1.jpg', '15_2.png', '15_3.png'),
(16, 6, 6, 'Cendo Lyteers Minidose', 19250, 0, NULL, 'Cendo Lyteers Minidose 5 0,6 ml', '2019-03-10 06:21:13', '16_1.jpg', '16_2.png', '16_3.png'),
(17, 8, 13, 'Curcuma+ Grow Strawberry', 21000, 0, NULL, 'Curcuma+ Grow Strawberry  200 ml', '2019-03-10 06:21:13', '17_1.jpg', '17_2.png', '17_3.png'),
(18, 3, 13, 'Enervon C', 27189, 0, NULL, 'Enervon C tablet 30', '2019-03-10 06:21:13', '18_1.jpg', '18_2.png', '18_3.png'),
(19, 3, 13, 'Enervon C FC', 92319, 0, NULL, 'Enervon C FC tablet 100', '2019-03-10 06:21:13', '19_1.jpg', '19_2.png', '19_3.png'),
(20, 8, 1, 'Enkasari Cairan 120 ml', 16237, 0, NULL, 'Enkasari Cairan 120 ml per botol', '2019-03-16 06:21:13', '20_1.jpg', '20_2.png', '20_3.png'),
(21, 10, 7, 'Insto Regular', 10600, 0, NULL, 'Insto Regular 7,5 ml', '2019-03-10 06:21:13', '21_1.jpg', '21_2.png', '21_3.png'),
(22, 9, 7, 'Iodyne Povidone 10% 30ml', 4500, 0, NULL, 'Iodyne Povidone 10% 30 ml', '2019-03-10 06:21:13', '22_1.jpg', '22_2.png', '22_3.png'),
(23, 9, 7, 'Iodyne Povidone 10% 60ml', 6550, 0, NULL, 'Iodyne Povidone 10% 60 ml', '2019-03-10 06:21:13', '23_1.jpg', '23_2.png', '23_3.png'),
(24, 9, 11, 'Lactacyd Baby', 20900, 0, NULL, 'Lactacyd Baby 60 ml', '2019-03-10 06:21:13', '24_1.jpg', '24_2.png', '24_3.png'),
(25, 9, 11, 'Lactacyd Woman', 19800, 0, NULL, 'Lactacyd Woman 60 ml', '2019-03-16 06:21:13', '25_1.jpg', '25_2.jpg', '25_3.png'),
(26, 8, 6, 'Magasida 60 ml Suspensi', 23545, 0, NULL, 'Magasida 60 ml Suspensi', '2019-03-10 06:21:13', '26_1.jpg', '26_2.png', '26_3.png'),
(27, 3, 6, 'Magasida Tablet', 82545, 0, NULL, 'Magasida Tablet 100 butir', '2019-03-10 06:21:13', '27_1.jpg', '27_2.jpg', '27_3.jpg'),
(28, 2, 11, 'Marcks Bedak Creme', 11560, 0, NULL, 'Marcks Bedak Creme', '2019-03-10 06:21:13', '28_1.jpg', '28_2.png', '28_3.png'),
(29, 2, 11, 'Marcks Bedak Rose', 11560, 0, NULL, 'Marcks Bedak Rose', '2019-03-10 06:21:13', '29_1.jpg', '29_2.jpg', '29_3.JPG'),
(30, 4, 7, 'Miconazole 2% 10 gr', 3250, 0, NULL, 'Miconazole 2% 10 gr (24 tube per dus)', '2019-03-16 06:21:13', '30_1.jpg', '30_2.png', '30_3.png'),
(31, 8, 6, 'Naprex Drops Tst', 36800, 0, NULL, 'Naprex Drops Tst 60 mg / 15 ml', '2019-03-10 06:21:13', '31_1.jpg', '31_2.png', '31_3.png'),
(32, 8, 6, 'Naprex Tst Rite', 34300, 0, NULL, 'Naprex Tst Rite 250 mg / 60 ml', '2019-03-10 06:21:13', '32_1.jpg', '32_2.png', '32_3.png'),
(33, 1, 13, 'Natur-E Advanced 16s', 32729, 0, NULL, 'Natur-E Advanced 16s 16 kapsul', '2019-03-10 06:21:13', '33_1.png', '33_2.jpg', '33_3.png'),
(34, 11, 11, 'Natur-E Advanced HBL 100 ml', 14832, 0, NULL, 'Natur-E Advanced HBL 100 ml', '2019-03-10 06:21:13', '34_1.jpg', '34_2.jpg', '34_3.png'),
(35, 11, 11, 'Natur-E Advanced HBL 245 ml', 27810, 0, NULL, 'Natur-E Advanced HBL 245 ml', '2019-03-16 06:21:13', '35_1.jpg', '35_2.jpg', '35_3.png'),
(36, 11, 12, 'Natur-E Daily Nourishing Energizing', 12220, 0, NULL, 'Natur-E Daily Nourishing Energizing 100 ml', '2019-03-10 06:21:13', '36_1.jpg', '36_2.jpg', '36_3.png'),
(37, 11, 11, 'Natur-E Daily NR Lot 100 ml', 12220, 0, NULL, 'Natur-E Daily NR Lot 100 ml', '2019-03-10 06:21:13', '37_1.png', '37_2.jpg', '37_3.jpg'),
(38, 11, 11, 'Natur-E Daily NR Lotion 245 ml', 24517, 0, NULL, 'Natur-E Daily Nourishing Lotion 245 ml', '2019-03-10 06:21:13', '38_1.jpg', '38_2.png', '38_3.png'),
(39, 11, 11, 'Natur-E HBL 100 ml', 10424, 0, NULL, 'Natur-E HBL 100 ml', '2019-03-10 06:21:13', '39_1.jpg', '39_2.png', '39_3.png'),
(40, 1, 13, 'Natur-E Soft Capsul', 15134, 0, NULL, 'Natur-E Soft Capsul 16', '2019-03-16 06:21:13', '40_1.jpg', '40_2.png', '40_3.png'),
(41, 8, 6, 'OBH Combi Batuk Berdahak Menthol', 8750, 0, NULL, 'OBH Combi Batuh Berdahak Menthol', '2019-03-10 06:21:13', '41_1.png', '41_2.png', '41_3.png'),
(42, 8, 6, 'OBH Combi Batuk Berdahak Menthol', 9200, 0, NULL, 'OBH Combi Batuk Berdahak Menthol\r\nNew Stock', '2019-03-10 06:21:13', '42_1.png', '42_2.png', '42_3.png'),
(43, 8, 7, 'OBH Combi Batuk Flu Menthol 60 ml', 9450, 0, NULL, 'OBH Combi Batuk Flu Menthol 60 ml', '2019-03-10 06:21:13', '43_1.jpg', '43_2.png', '43_3.png'),
(44, 8, 7, 'OBH Combi Batuk Flu Menthol 100 ml', 12500, 0, NULL, 'OBH Combi Batuk Flu Menthol 100 ml', '2019-03-10 06:21:13', '44_1.jpg', '44_2.png', '44_3.png'),
(45, 11, 7, 'Peditox 1% 50 ml', 5550, 0, NULL, 'Peditox 1% 50 ml adalah larutan/lotion pembasmi kutu rambut', '2019-03-16 06:21:13', '45_1.jpg', '45_2.jpg', '45_3.png'),
(46, 11, 7, 'Peditox 1% 50 ml (new stock)', 6000, 0, NULL, 'Peditox 1% 50ml adalah larutan pembasmi kutu rambut', '2019-03-10 06:21:13', '46_1.jpg', '46_2.jpg', '46_3.png'),
(47, 1, 13, 'Pharmaton Formula', 191126, 0, NULL, 'Pharmaton Formula 50 kapsul per dus', '2019-03-10 06:21:13', '47_1.jpg', '47_2.png', '47_3.png'),
(48, 10, 7, 'Rohto Cool', 13320, 0, NULL, 'Rohto Cool 7 ml', '2019-03-10 06:21:13', '48_1.jpg', '48_2.png', '48_3.png'),
(49, 9, 6, 'Salicyl Fresh Kemasan Baru', 12380, 0, NULL, 'Salicyl Fresh 60 gr', '2019-03-10 06:21:13', '49_1.jpg', '49_2.png', '49_3.png'),
(50, 8, 1, 'Silex Syrup', 45413, 0, NULL, 'Silex Syrup 100 ml', '2019-03-16 06:21:13', '50_1.jpg', '50_2.jpg', '50_3.png'),
(51, 3, 13, 'Vicee Orange', 57173, 0, NULL, 'Vicee Orange 100 tablet per dus', '2019-03-10 06:21:13', '51_1.jpg', '51_2.jpg', '51_3.jpg'),
(53, 3, 6, 'Vitamin C 100 mg', 11040, 0, NULL, 'Vitamin C 100 mg', '2019-03-10 06:21:13', '52_1.jpg', '52_2.jpg', '52_3.png'),
(54, 7, 7, 'Antihemoroid', 40000, 0, NULL, '10 Suppository per dus', '2019-03-10 06:21:13', '1_1.jpg', '1_2.png', '1_3.png'),
(55, 7, 7, 'Antihemoroid', 50000, 0, NULL, '10 Suppository per dus', '2019-03-10 06:21:13', '1_1.jpg', '1_2.png', '1_3.png'),
(56, 7, 7, 'Antihemoroid', 60000, 0, NULL, '10 Suppository per dus', '2019-03-10 06:21:13', '1_1.jpg', '1_2.png', '1_3.png');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `kode_status` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`kode_status`, `status`) VALUES
(7, 'Barang Diretur'),
(6, 'Barang Diterima'),
(1, 'Menunggu Konfirmasi'),
(2, 'Menunggu Pembayaran'),
(3, 'Pembayaran Diterima'),
(4, 'Pemesanan Dibatalkan'),
(5, 'Proses Pengiriman');

-- --------------------------------------------------------

--
-- Table structure for table `status_pemesanan`
--

CREATE TABLE `status_pemesanan` (
  `id_status_pemesanan` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `kode_status` int(11) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_pemesanan`
--

INSERT INTO `status_pemesanan` (`id_status_pemesanan`, `id_pemesanan`, `kode_status`, `waktu`, `keterangan`) VALUES
(1, 3, 1, '2019-02-28 20:13:58', NULL),
(2, 4, 1, '2019-03-02 04:06:19', NULL),
(3, 5, 1, '2019-03-16 16:40:50', NULL),
(5, 5, 2, '2019-03-16 19:10:08', NULL),
(6, 5, 3, '2019-03-16 19:13:40', NULL),
(7, 5, 5, '2019-03-16 19:13:44', NULL),
(8, 5, 6, '2019-03-16 19:13:46', NULL),
(9, 6, 1, '2019-03-16 20:37:15', NULL),
(10, 7, 1, '2019-03-18 19:25:33', NULL),
(11, 8, 1, '2019-03-18 19:28:21', NULL),
(12, 9, 1, '2019-03-18 19:29:42', NULL),
(13, 9, 4, '2019-03-18 19:30:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id_user` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `apotek`
--
ALTER TABLE `apotek`
  ADD PRIMARY KEY (`id_apotek`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `detil_pemesanan`
--
ALTER TABLE `detil_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`,`id_produk`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  ADD PRIMARY KEY (`kode_jenis`);

--
-- Indexes for table `kategori_kuisioner`
--
ALTER TABLE `kategori_kuisioner`
  ADD PRIMARY KEY (`id_kategori_kuisioner`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`kode_kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD PRIMARY KEY (`id_pertanyaan`),
  ADD KEY `id_dimensi` (`id_kategori_kuisioner`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `kode_jenis` (`kode_jenis`),
  ADD KEY `kode_kategori` (`kode_kategori`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`kode_status`),
  ADD UNIQUE KEY `status` (`status`);

--
-- Indexes for table `status_pemesanan`
--
ALTER TABLE `status_pemesanan`
  ADD PRIMARY KEY (`id_status_pemesanan`),
  ADD KEY `id_pemesanan` (`id_pemesanan`),
  ADD KEY `kode_status` (`kode_status`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_produk` (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apotek`
--
ALTER TABLE `apotek`
  MODIFY `id_apotek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  MODIFY `kode_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kategori_kuisioner`
--
ALTER TABLE `kategori_kuisioner`
  MODIFY `id_kategori_kuisioner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `kode_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kuisioner`
--
ALTER TABLE `kuisioner`
  MODIFY `id_pertanyaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `kode_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status_pemesanan`
--
ALTER TABLE `status_pemesanan`
  MODIFY `id_status_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apotek`
--
ALTER TABLE `apotek`
  ADD CONSTRAINT `apotek_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `pelanggan` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `detil_pemesanan`
--
ALTER TABLE `detil_pemesanan`
  ADD CONSTRAINT `detil_pemesanan_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id_pemesanan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `detil_pemesanan_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `pelanggan` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `keranjang_ibfk_3` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON UPDATE CASCADE;

--
-- Constraints for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD CONSTRAINT `kuisioner_ibfk_1` FOREIGN KEY (`id_kategori_kuisioner`) REFERENCES `kategori_kuisioner` (`id_kategori_kuisioner`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`kode_kategori`) REFERENCES `kategori_produk` (`kode_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`kode_jenis`) REFERENCES `jenis_produk` (`kode_jenis`) ON UPDATE CASCADE;

--
-- Constraints for table `status_pemesanan`
--
ALTER TABLE `status_pemesanan`
  ADD CONSTRAINT `status_pemesanan_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id_pemesanan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `status_pemesanan_ibfk_2` FOREIGN KEY (`kode_status`) REFERENCES `status` (`kode_status`) ON UPDATE CASCADE;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `pelanggan` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
